/* OwlGalunggung Code Editor
 * ipc_owlgal2owlgal.h - IPC socket owlgalunggung to owlgalunggung
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __IPC_OWLGAL2OWLGAL_H_
#define __IPC_OWLGAL2OWLGAL_H_

gboolean ipc_owlgal2owlgal_start(GList * filenames, gboolean in_new_window);
void ipc_owlgal2owlgal_cleanup(void);

#endif /* __IPC_OWLGAL2OWLGAL_H_ */
