/* OwlGalunggung Code Editor
 * owlgaltextview2_scanner.h
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* for the design docs see owlgaltextview2.h */
#ifndef _OWLGALTEXTVIEW2_SCANNER_H_
#define _OWLGALTEXTVIEW2_SCANNER_H_

#include "owlgaltextview2.h"

GQueue *get_contextstack_at_position(OwlGalunggungTextView * btv, GtkTextIter * position);
Tfound *get_foundcache_next(OwlGalunggungTextView * bt2, GSequenceIter ** siter);
Tfound *get_foundcache_first(OwlGalunggungTextView * bt2, GSequenceIter ** retsiter);
Tfound *get_foundcache_at_offset(OwlGalunggungTextView * btv, guint offset, GSequenceIter ** retsiter);
void foundcache_update_offsets(OwlGalunggungTextView * btv, guint startpos, gint offset);
Tfoundblock *pop_blocks(gint numchange, Tfoundblock * curblock);
gboolean test_condition(Tfoundcontext *curfcontext, Tfoundblock *curfblock, Tpattern_condition *pcond);
gboolean owlgaltextview2_run_scanner(OwlGalunggungTextView * btv, GtkTextIter * visible_end);
void scan_for_prefix_start(OwlGalunggungTextView * btv, guint16 contextnum, GtkTextIter * start,
						   GtkTextIter * cursor);
void scan_for_autocomp_prefix(OwlGalunggungTextView * btv, GtkTextIter * mstart, GtkTextIter * cursorpos,
						 gint * contextnum);
guint scan_for_tooltip(OwlGalunggungTextView * btv, GtkTextIter * mstart, GtkTextIter * position,
						  gint * contextnum);
void cleanup_scanner(OwlGalunggungTextView * btv);
void scancache_destroy(OwlGalunggungTextView * btv);

#endif
