/* OwlGalunggung Code Editor
 * about.c - the About dialog
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gmodule.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>				/* getopt() */

#ifdef WIN32
#include <windows.h>
#include <shellapi.h>			/* ShellExecute */
#endif							/* WIN32 */

#include "about.h"
#include "about_rev.h"

#include "../owl_config.h"
#include "../plugins.h"
#include "../owlgalunggung.h"		/* OWLGALUNGGUNG_SPLASH_FILENAME */


static void
owlgalunggung_url_show(const gchar * url)
{
#ifdef WIN32
	if ((int) ShellExecute(NULL, "open", url, NULL, NULL, SW_SHOWNORMAL) <= 32) {
		g_warning("Failed trying to launch URL in about dialog");
	}
#else
#ifdef PLATFORM_DARWIN
	GString *string;
	string = g_string_new("open \"");
	string = g_string_append(string, url);
	string = g_string_append(string, "\"");
	g_print("owlgalunggung_url_show, launching uri=%s\n", string->str);
	system(string->str);
	g_string_free(string, TRUE);
#else
	GError *error = NULL;

	g_app_info_launch_default_for_uri(url, NULL, &error);
	if (error) {
		g_warning("owlgalunggung_url_show, %s", error->message);
		g_error_free(error);
	}
#endif
#endif
}

static void
about_options_dialog_create(GtkAction * action, gpointer user_data)
{
	GtkWidget *dialog;
	gchar *sec_text;

	dialog =
		gtk_message_dialog_new(GTK_WINDOW(OWLGALWIN(user_data)->main_window), GTK_DIALOG_DESTROY_WITH_PARENT,
							   GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE,
#ifdef SVN_REVISION
							   PACKAGE_STRING " rev" SVN_REVISION);
#else	/* SVN_REVISION */
							   PACKAGE_STRING);
#endif	/* SVN_REVISION */
	sec_text = g_strconcat(_("This version of OwlGalunggung was built with:\n"), CONFIGURE_OPTIONS, NULL);

	gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(dialog),
			"%s\ngtk %d.%d.%d (runtime gtk %d.%d.%d)\nglib %d.%d.%d (runtime %d.%d.%d)\n"
			"with libenchant... %s\nwith libenchant >= 1.4... %s\n"
			"with libgucharmap... %s\nwith libgucharmap_2... %s\n"
			"with hyang... %s"
			, sec_text
			, GTK_MAJOR_VERSION, GTK_MINOR_VERSION, GTK_MICRO_VERSION
			, gtk_major_version, gtk_minor_version, gtk_micro_version
			, GLIB_MAJOR_VERSION, GLIB_MINOR_VERSION, GLIB_MICRO_VERSION
			, glib_major_version, glib_minor_version, glib_micro_version
#ifdef HAVE_LIBENCHANT
			, "yes"
#else
			, "no"
#endif
#ifdef HAVE_LIBENCHANT_1_4
			, "yes"
#else
			, "no"
#endif
#ifdef HAVE_LIBGUCHARMAP
			, "yes"
#else
			, "no"
#endif
#ifdef HAVE_LIBGUCHARMAP_2
			, "yes"
#else
			, "no"
#endif
#ifdef HAVE_HYANG
			, "yes"
#else
			, "no"
#endif
			);

	g_free(sec_text);

	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

static void
about_report_bug(GtkAction * action, gpointer user_data)
{
	GString *string;
	gchar *options;

	string = g_string_new("https://github.com/owlgalunggung/owlgalunggung/issues");
#ifdef WIN32
	string = g_string_append(string, ";op_sys=Windows");
#elif PLATFORM_DARWIN
	string = g_string_append(string, ";op_sys=Mac%20OS");
#endif	/* WIN32 */
	string = g_string_append(string, ";version=");
#ifdef SVN_REVISION
	string = g_string_append_uri_escaped(string, "development (SVN TRUNK)",NULL,FALSE);
#else	/* SVN_REVISION */
	string = g_string_append(string, PACKAGE_VERSION);
#endif	/* SVN_REVISION */
	string = g_string_append(string, ";comment=");
	options = g_strconcat(
#ifdef SVN_REVISION
						  "SVN revision ", SVN_REVISION, "\n",
#endif	/* SVN_REVISION */
						  "OwlGalunggung was configured with: ", CONFIGURE_OPTIONS, "\n", NULL);

	string = g_string_append_uri_escaped(string, options, NULL, FALSE);
	g_free(options);

	owlgalunggung_url_show(string->str);
	g_string_free(string, TRUE);
}

static void
about_show_homepage(GtkAction * action, gpointer user_data)
{
	owlgalunggung_url_show("http://www.owlgalunggung.org/download/");
}

#if !GTK_CHECK_VERSION(3,0,0)
static void
about_activate_url(GtkAboutDialog * about, const gchar * url, gpointer data)
{
	owlgalunggung_url_show(url);
}
#endif

#ifdef MAC_INTEGRATION
static gboolean
activate_link_lcb (GtkWidget   *dialog,
               const gchar *url,
               gpointer     data)
{
  owlgalunggung_url_show(url);
  return TRUE;
}
#endif

static void
about_dialog_create(GtkAction * action, gpointer user_data)
{
	Towlgalwin *owlgalwin = OWLGALWIN(user_data);
	GdkPixbuf *logo;

	const gchar *artists[] = {
		"Dian Saphira <dshaphira@owlgalunggung.org>",
		"Rida Farida <rida@owlgalunggung.org>\n",
		NULL
	};

	const gchar *authors[] = {
		"Hilman P. Alisabana <alisabana@hyang.org> (Project leader)",
		"M. Sindu Natama <sindutama@owlgalunggung.org>",
		"Yazid Amini <yazidiamin@owlgalunggung.org>",
		"Elvira Larasati <laras@owlgalunggung.org>",
		"Nadya <nadya@owlgalunggung.org>",
		"Heru Yonardi <heru@owlgalunggung.org>",
	    "Dian Saphira <dshaphira@owlgalunggung.org>",
		_("\nIf you know of anyone missing from this list,\nplease let us know at:"),
		_("contributing@owlgalunggung.org"),
		_("\nThanks to all who helped make this software available.\n"),
		NULL
	};

	const gchar *documenters[] = {
		"Hilman P. Alisabana <alisabana@hyang.org>",
		"M. Sindu Natama <sindutama@owlgalunggung.org>",
		"Yazid Amini <yazidiamin@owlgalunggung.org>",
		"Elvira Larasati <laras@owlgalunggung.org>",
		"Nadya <nadya@owlgalunggung.org>",
		"Heru Yonardi <heru@owlgalunggung.org>",
		"Dara Irawan <dari@owlgalunggung.org>\n",
		NULL
	};

	const gchar *copyright = "Copyright \xc2\xa9 2017-2018 Hyang Language Foundation.\n";

	const gchar *license =
		"This program is free software; you can redistribute it and/or modify it "
		"under the terms of the GNU General Public License as published by "
		"the Free Software Foundation; either version 3 of the License, or "
		"(at your option) any later version.\n"
		"\n"
		"This program is distributed in the hope that it will be useful, "
		"but WITHOUT ANY WARRANTY; without even the implied warranty of "
		"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the "
		"GNU General Public License for more details.\n"
		"\n"
		"You should have received a copy of the GNU General Public License "
		"along with this program.  If not, see http://www.gnu.org/licenses/ .";

	const gchar *comments =
		_
		("An open-source code editor for Hyang language designers and developers, supporting many programming and markup languages, but focusing on creating Hyang development environment.");

	const gchar *translator_credits = _("translator-credits");

	{
		GError *error = NULL;
		logo = gdk_pixbuf_new_from_file(OWLGALUNGGUNG_SPLASH_FILENAME, &error);
		if (error) {
			g_print("ERROR while loading splash screen: %s\n", error->message);
			g_error_free(error);
		}
	}

#if !GTK_CHECK_VERSION(3, 0, 0)
	gtk_about_dialog_set_url_hook(about_activate_url, NULL, NULL);
#endif /* gtk3 */
#ifndef MAC_INTEGRATION
	gtk_show_about_dialog(GTK_WINDOW(owlgalwin->main_window), "logo", logo, "name", PACKAGE,
#ifdef SVN_REVISION
						  "version", VERSION " rev" SVN_REVISION,
#else	/* SVN_REVISION */
						  "version", VERSION,
#endif	/* SVN_REVISION */
						  "comments", comments,
						  "copyright", copyright,
						  "license", license,
						  "website", "http://www.owlgalunggung.org/download/",
						  "authors", authors,
						  "artists", artists,
						  "documenters", documenters,
						  "translator_credits", translator_credits,
						  "wrap-license", TRUE,
						  NULL);

	if (logo)
		g_object_unref(logo);
#else
	GtkWidget *dialog;
	dialog = gtk_about_dialog_new();
	gtk_window_set_transient_for(GTK_WINDOW( dialog ), GTK_WINDOW(owlgalwin->main_window));
	gtk_window_set_destroy_with_parent (GTK_WINDOW(dialog), TRUE);
	g_signal_connect (dialog, "activate-link", G_CALLBACK (activate_link_lcb), NULL);
         /* Set it's properties */
	gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(dialog), "OwlGalunggung" );
	gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog),
#ifdef SVN_REVISION
						  VERSION " rev" SVN_REVISION
#else	/* SVN_REVISION */
					  VERSION
#endif	/* SVN_REVISION */
	 );
	gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), copyright);
	gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(dialog), "http://www.owlgalunggung.org/download/" );
	gtk_about_dialog_set_authors(GTK_ABOUT_DIALOG(dialog), authors);
	gtk_about_dialog_set_artists(GTK_ABOUT_DIALOG(dialog), artists);
	gtk_about_dialog_set_documenters(GTK_ABOUT_DIALOG(dialog), documenters);
	gtk_about_dialog_set_license(GTK_ABOUT_DIALOG(dialog), license);
	gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), comments);
	gtk_about_dialog_set_translator_credits(GTK_ABOUT_DIALOG(dialog), translator_credits);
	gtk_about_dialog_set_wrap_license(GTK_ABOUT_DIALOG(dialog), TRUE);
	gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(dialog), logo);
	g_object_unref(logo), logo=NULL;
    /* Run dialog and destroy it after it returns. */
    gtk_dialog_run( GTK_DIALOG(dialog) );
    gtk_widget_destroy(GTK_WIDGET(dialog));
#endif /* MAC_INTEGRATION */
}

static void
about_init(void)
{
#ifdef ENABLE_NLS
	DEBUG_MSG("about_init, gettext domain-name=%s\n", PACKAGE "_plugin_about");
	bindtextdomain(PACKAGE "_plugin_about", LOCALEDIR);
	bind_textdomain_codeset(PACKAGE "_plugin_about", "UTF-8");
#endif							/* ENABLE_NLS */
}

static const gchar *about_plugin_ui =
	"<ui>"
	"  <menubar name='MainMenu'>"
	"    <menu action='HelpMenu'>"
	"      <menuitem action='HelpHomepage'/>"
	"      <menuitem action='HelpReportBug'/>"
	"      <separator/>"
	"      <menuitem action='HelpAbout'/>"
	"      <menuitem action='HelpBuildInfo'/>"
	"    </menu>"
	"  </menubar>"
	"</ui>";

static void
about_initgui(Towlgalwin * owlgalwin)
{
	GtkActionGroup *action_group;
	GError *error = NULL;

	static const GtkActionEntry about_actions[] = {
		{"HelpMenu", NULL, N_("_Help")},
		{"HelpHomepage", GTK_STOCK_HOME, N_("OwlGalunggung _Homepage"), NULL, N_("OwlGalunggung homepage"),
		 G_CALLBACK(about_show_homepage)},
		{"HelpReportBug", NULL, N_("Report a _Bug"), NULL, N_("Report a bug"), G_CALLBACK(about_report_bug)},
		{"HelpAbout", GTK_STOCK_ABOUT, N_("_About"), NULL, N_("About OwlGalunggung"),
		 G_CALLBACK(about_dialog_create)},
		{"HelpBuildInfo", GTK_STOCK_INFO, N_("Build _Info"), NULL, N_("Build info"),
		 G_CALLBACK(about_options_dialog_create)}
	};

	action_group = gtk_action_group_new("AboutActions");
	gtk_action_group_set_translation_domain(action_group, GETTEXT_PACKAGE "_plugin_about");
	gtk_action_group_add_actions(action_group, about_actions, G_N_ELEMENTS(about_actions), owlgalwin);
	gtk_ui_manager_insert_action_group(owlgalwin->uimanager, action_group, 0);
	g_object_unref(action_group);

	gtk_ui_manager_add_ui_from_string(owlgalwin->uimanager, about_plugin_ui, -1, &error);
	if (error != NULL) {
		g_warning("building about plugin menu failed: %s", error->message);
		g_error_free(error);
	}
}

G_MODULE_EXPORT TOwlGalunggungPlugin *
getplugin(void)
{
	static TOwlGalunggungPlugin owlgalplugin = {
		"About Dialog",
		OWLGALPLUGIN_VERSION,
		GTK_MAJOR_VERSION,
		sizeof(Tdocument),
		sizeof(Tsessionvars),
		sizeof(Tglobalsession),
		sizeof(Towlgalwin),
		sizeof(Tproject),
		sizeof(Tmain),
		sizeof(Tproperties),
		OWLGALPLUGIN_PRIORITY_LAST,
		1,
		NULL,						/* private */
		about_init,					/* init */
		about_initgui,
		NULL /*about_enforce_session*/,
		NULL /*about_cleanup*/,
		NULL /*about_cleanup_gui*/,
		NULL /*about_register_globses_config*/,
		NULL /*about_register_session_config*/,
		NULL,						/* binary compatibility */
		NULL,
		NULL,
		NULL
	};
	return &owlgalplugin;
}
