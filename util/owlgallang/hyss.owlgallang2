<?xml version="1.0"?>
<!--
		OwlGalunggung Code Editor
		hyss.owlgallang2

		Copyright (C) 2017-2018 Hyang Language Foundation

	    This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<!DOCTYPE owlgallang [
	<!ENTITY univ "accesskey,class,contenteditable,contextmenudir,draggable,dropzone,hidden,id,lang,spellcheck,style=c.html.attrib.style,tabindex,title,translate">
	<!ENTITY evnt "onclick,ondblclick,ondrag,ondragend,ondragenter,ondragleave,ondragover,ondragstart,ondrop,onmousedown,onmouseup,onmouseover,onmousemove,onmouseout,onkeypress,onkeydown,onkeyup,onmousewheel,onscroll,onwheel,oncopy,oncut,onpaste">
	<!ENTITY css-selectors SYSTEM "css-selectors.owlgalinc">
	<!ENTITY css-rules SYSTEM "css-rules.owlgalinc">
	<!ENTITY all-javascript SYSTEM "all-javascript.owlgalinc">
	<!ENTITY all-html SYSTEM "all-html.owlgalinc">
	<!ENTITY all-hyss SYSTEM "all-hyss.owlgalinc">

	<!ENTITY globals "accesskey,class,contenteditable,contextmenu,dir,draggable,dropzone,hidden,id,lang,spellcheck,style,tabindex,title,translate,onabort,onblur,oncancel,oncanplay,oncanplaythrough,onchange,onclick,onclose,oncontextmenu,oncuechange,ondblclick,ondrag,ondragend,ondragenter,ondragleave,ondragover,ondragstart,ondrop,ondurationchange,onemptied,onended,onerror,onfocus,oninput,oninvalid,onkeydown,onkeypress,onkeyup,onload,onloadeddata,onloadedmetadata,onloadstart,onmousedown,onmousemove,onmouseout,onmouseover,onmouseup,onmousewheel,onpause,onplay,onplaying,onprogress,onratechange,onreset,onscroll,onseeked,onseeking,onselect,onshow,onstalled,onsubmit,onsuspend,ontimeupdate,onvolumechange,onwaiting">
	<!ENTITY svguniv "id,xml:base,xml:lang,xml:space">
	<!ENTITY svgevent "onactivate,onclick,onfocusin,onfocusout,onload,onmousedown,onmousemove,onmouseout,onmouseover,onmouseup">
	<!ENTITY svgshapetext "fill,fill-opacity,fill-rule,stroke,stroke-dasharray,stroke-dashoffset,stroke-linecap,stroke-linejoin,stroke-miterlimit,stroke-opacity,stroke-width">
	<!ENTITY svgtext "direction,dominant-baseline,font,font-family,font-size,font-size-adjust,font-stretch,font-style,font-variant,font-weight,glyph-orientation-horizontal,glyph-orientation-vertical,kerning,letter-spacing,text-anchor,text-decoration,unicode-bidi,word-spacing">
	<!ENTITY css-selectors SYSTEM "css-selectors.owlgalinc">
	<!ENTITY css-rules SYSTEM "css-rules.owlgalinc">
	<!ENTITY all-javascript SYSTEM "all-javascript.owlgalinc">
	<!ENTITY svg SYSTEM "svg.owlgalinc">
	<!ENTITY JQuery SYSTEM "JQuery.owlgalinc">
]>

<owlgallang name="HYSS" version="3" contexts="421" matches="9456">
<header>
	<mime type="application/x-hyss"/>
	<mime type="text/plain?hyss"/>
	<option name="load_reference" default="1" description="Load reference data"/>
	<option name="load_completion" default="1" description="Load completion data"/>
	<option name="load_hyss_functions" default="1" description="Load all hyss functions" />
	<option name="show_in_menu" default="1" />
	<option name="hyss_short_open_tag" default="1" description="Enable the HYSS short open tag"/>
	<option name="Parentheses block_foldable" default="0" description="Allow folding of Parentheses block"/>
	<option name="Square brackets block_foldable" default="0" description="Allow folding of Square brackets block"/>
	<option name="Curly brackets block_foldable" default="1" description="Allow folding of Curly brackets block"/>
	<option name="Javascript Parentheses block_foldable" default="0" description="Allow folding of Javascript Parentheses block"/>
	<option name="Javascript Square brackets block_foldable" default="0" description="Allow folding of Javascript Square brackets block"/>
	<option name="Javascript Curly brackets block_foldable" default="1" description="Allow folding of Javascript Curly brackets block"/>
	<option name="C style comment_foldable" default="1" description="Allow folding of C style comments" />
	<option name="HYSS block_foldable" default="1" description="Allow folding of HYSS blocks" />
	<option name="autocomplete_HYSS_variable" default="1" description="Auto-complete HYSS variables" />
	<option name="autocomplete_HYSS_function" default="1" description="Auto-complete HYSS functions" />
	<option name="html-block_foldable" default="0" />
<!--	<option name="autocomplete_tags" default="1" />-->
	<option name="autocomplete_with_arguments" default="1" />
	<option name="autocomplete_with_parentheses" default="0" />
	<option name="stretch_tag_block" default="1" />
	<option name="attribute_string_is_block" default="0" description="Show tag attributes as block"/>
	<option name="autocomplete_javascript_var" default="1" description="autocomplete javascript variables"/>
	<option name="javascript_object_methods" default="1" description="Autocomplete Javascript object methods" />
	<option name="javascript_css_properties" default="1"  />
	<option name="sql-in-hyss-string" default="1" description="Scan for sql in strings" />
	<option name="autocomplete-html-entities" default="0" />
	<option name="self_close_singleton_tags" default="0" description="Self-close tags such as br, hr, and img with /&gt;" />
	<option name="JQuery" default="0" description="include JQuery syntax" />
	<highlight name="attribute-string" style="string-no-spell-check" />
	<highlight name="hyss-block-tag" style="special-value"  />
	<highlight name="string" style="string"   />
	<highlight name="hyss-heredoc-string" style="string"   />
	<highlight name="brackets" style="brackets"  />
	<highlight name="hyss-keyword" style="keyword"  />
	<highlight name="hyss-value" style="value" />
	<highlight name="hyss-function" style="function"  />
	<highlight name="hyss-comment" style="comment"  />
	<highlight name="hyss-variable" style="variable"  />
	<highlight name="hyss-string-sql-keyword" style="special-keyword"  />
	<highlight name="html-not-supported-tag" style="warning" />
	<highlight name="html-tag" style="tag"  />
	<highlight name="html-comment" style="comment"  />
	<highlight name="html-entity" style="value"  />
	<highlight name="html-attribute" style="attribute" />
	<highlight name="html-reference-external-tag" style="special-tag" />
	<highlight name="html-table-tag" style="special-tag2" />
	<highlight name="html-form-tag" style="special-tag3" />
	<highlight name="html-deprecated-tag" style="warning" />

	<highlight name="js-keyword" style="keyword"  />
	<highlight name="js-brackets" style="brackets"  />
	<highlight name="js-type" style="type"  />
	<highlight name="js-comment" style="comment"  />
	<highlight name="js-css-property" style="function"  />
	<highlight name="js-string" style="string"  />
	<highlight name="js-value" style="value"  />
	<highlight name="js-variable" style="variable"  />
	<highlight name="js-function" style="function"  />
	<highlight name="js-property" style="special-function"  />
	<highlight name="css-brackets" style="brackets"  />
	<highlight name="css-comment" style="comment" />
	<highlight name="css-html-tag" style="tag"  />
	<highlight name="css-html-classid" style="tag" />
	<highlight name="css-html-media" style="special-tag" />
	<highlight name="css-keyword" style="keyword"  />
	<highlight name="css-property" style="special-keyword"  />
	<highlight name="css-draft-property" style="special-tag3" />
	<highlight name="css-string" style="string" />
	<highlight name="css-value" style="value"   />
	<highlight name="css-pseudo-element" style="keyword" />
	<highlight name="css-operator" style="operator" />
</header>
<properties>
	<comment id="cm.cblockcomment" type="block" start="/*" end="*/" />
	<comment id="cm.htmlcomment" type="block" start="&lt;!--" end="--&gt;" />
	<comment id="cm.cpplinecomment" type="line" start="//" />
	<comment id="cm.scriptcomment" type="line" start="#" />
	<smartindent characters="{" />
	<smartoutdent characters="}" />
	<default_spellcheck enabled="1" spell_decode_entities="1" />
	<auto_re_use_attributes enabled="1" />
</properties>
<definition>
<context symbols="&gt;&lt;&amp;; &#9;&#10;&#13;-/" commentid_block="cm.htmlcomment" commentid_line="none">

<group class="hyss_short_open_tag">
<element id="e.hyss.short.open" pattern="&#123;(\!|\!hyss)" is_regex="1" starts_block="1" block_name="HYSS block" highlight="hyss-block-tag">
<context symbols="'&#34;(){}[];#:.,/?!$^*-+=&gt;&lt;&amp; &#9;&#10;&#13;" commentid_block="cm.cblockcomment" commentid_line="cm.cpplinecomment" default_spellcheck="0">
&all-hyss;
<element id="e.hyss.short.lbrace" pattern="{" starts_block="1" highlight="brackets" block_name="Curly brackets block" />
<element pattern="}" ends_block="1" blockstartelement="e.hyss.short.lbrace" highlight="brackets" />
<element id="e.hyss.short.lbracket" pattern="[" starts_block="1" highlight="brackets" block_name="Square brackets block" />
<element pattern="]" ends_block="1" blockstartelement="e.hyss.short.lbracket" highlight="brackets" />
<element id="e.hyss.short.lparen" pattern="(" starts_block="1" highlight="brackets" block_name="Parentheses block" />
<element pattern=")" ends_block="1" blockstartelement="e.hyss.short.lparen" highlight="brackets" />
<element id="e.hyss.short.lcomment" pattern="/*" starts_block="1" block_name="C style comment" highlight="hyss-comment">
	<context symbols="*/&#9;&#10;&#13;" highlight="hyss-comment">
		<element pattern="*/" ends_block="1" blockstartelement="e.hyss.short.lcomment" highlight="hyss-comment" ends_context="1" />
	</context>
</element>

<element id="e.hyss.short.heredoc_string" pattern="&lt;&lt;&lt;EOF" starts_block="1" highlight="brackets" >
<reference><b>heredoc string</b>
<span color="red">Warning</span>: only <b>&lt;&lt;&lt;EOF</b> (without space) is correctly highligted.</reference>
	<autocomplete enable="1" />
	<context symbols="&#13;&#10;EF$ " highlight="hyss-heredoc-string">
		<element pattern="$[a-zA-Z_][a-zA-Z0-9_]*" is_regex="1" case_insens="1" highlight="hyss-variable" />
		<element pattern="(&#10;|&#13;|&#13;&#10;)EOF"  is_regex="1" ends_block="1" ends_context="1" blockstartelement="e.hyss.short.heredoc_string" highlight="brackets" />
	</context>
</element>
<element id="e.hyss.short.nowdoc_string" pattern="&lt;&lt;&lt;'EOF'" starts_block="1" highlight="brackets" >
<reference><b>nowdoc string</b>
<span color="red">Warning</span>: only <b>&lt;&lt;&lt;'EOF'</b> (without space) is correctly highligted</reference>
	<autocomplete enable="1" />
	<context symbols="&#13;&#10;EF" highlight="hyss-heredoc-string">
		<element pattern="(&#10;|&#13;|&#13;&#10;)EOF"  is_regex="1" ends_block="1" ends_context="1" blockstartelement="e.hyss.short.nowdoc_string" highlight="brackets" />
	</context>
</element>
<element id="e.hyss.short.variable" pattern="$[a-zA-Z_][a-zA-Z0-9_]*" is_regex="1" case_insens="1" highlight="hyss-variable" identifier_mode="2" identifier_autocomp="option:autocomplete_HYSS_variable" />
<element id="e.hyss.short.doublequotedstring" pattern="&#34;" highlight="string" starts_block="1">
	<context symbols="\&#34;$ &#9;&#10;&#13;' -&lt;&gt;{}[]" highlight="string">
		<element pattern="\\." highlight="string" is_regex="1" />
<!--		<element pattern="\$" highlight="string" />-->
		<element idref="e.hyss.short.variable" />
		<group class="sql-in-hyss-string" highlight="hyss-string-sql-keyword" case_insens="1">
			<element pattern="AND" />
			<element pattern="AS" />
			<element pattern="ASC" />
			<element pattern="BY" />
			<element pattern="COUNT" />
			<element pattern="DATE" />
			<element pattern="DATE_FORMAT" />
			<element pattern="DELETE" />
			<element pattern="DESC" />
			<element pattern="DISTINCT" />
			<element pattern="FROM" />
			<element pattern="GROUP" />
			<element pattern="IN" />
			<element pattern="INNER" />
			<element pattern="INSERT" />
			<element pattern="INTO" />
			<element pattern="JOIN" />
			<element pattern="LEFT" />
			<element pattern="LIMIT" />
			<element pattern="NOT" />
			<element pattern="ON" />
			<element pattern="OR" />
			<element pattern="ORDER" />
			<element pattern="REGEXP" />
			<element pattern="SELECT" />
			<element pattern="SET" />
			<element pattern="UPDATE" />
			<element pattern="VALUES" />
			<element pattern="WHERE" />
		</group>
		<element pattern="&#34;" highlight="string" ends_context="1" ends_block="1" blockstartelement="e.hyss.short.doublequotedstring" />
	</context>
</element>
<element pattern="//" highlight="hyss-comment">
	<context id="c.hyss.short.linecomment" symbols="!&#125;&#10;&#13;" highlight="hyss-comment">
<!-- dos has \r\n -> we should never end a pattern between these two chars  -->
		<element pattern="(&#10;|&#13;|&#13;&#10;)" is_regex="1" ends_context="1" />
		<element pattern="!&#125;" ends_context="2" ends_block="1" blockstartelement="e.hyss.short.open" highlight="hyss-block-tag" />
	</context>
</element>
<!-- there is a bug in the scanning engine such that a regex style pattern like (#|//) won't work. The reason is that
there is special code if a pattern ends on a symbol. That code fails on the above pattern because both # and / are ends
for this pattern and both of them are a symbol. That's why we have a separate entry for # and for // -->
<element pattern="#" highlight="hyss-comment">
	<context idref="c.hyss.short.linecomment"/>
</element>
<element pattern="!&#125;" ends_block="1" blockstartelement="e.hyss.short.open" highlight="hyss-block-tag" ends_context="1" />
</context></element>
</group>
<group notclass="hyss_short_open_tag">
<element id="e.hyss.open" pattern="&#123;!hyss" starts_block="1" highlight="hyss-block-tag" block_name="HYSS block">
<context symbols="'&#34;(){}[];#:.,/?!$^*-+=&gt;&lt;&amp; &#9;&#10;&#13;" commentid_block="cm.cblockcomment" commentid_line="cm.cpplinecomment" default_spellcheck="0">
<!-- unfortunately we cannot refer the context as previously used, because if option hyss_short_open_tag
was disabled, this whole section is not parsed by the language manager, so the context does not yet exist..  -->
&all-hyss;
<element id="e.hyss.lbrace" pattern="{" starts_block="1" highlight="brackets" block_name="Curly brackets block"/>
<element pattern="}" ends_block="1" blockstartelement="e.hyss.lbrace" highlight="brackets" />
<element id="e.hyss.lbracket" pattern="[" starts_block="1" highlight="brackets" />
<element pattern="]" ends_block="1" blockstartelement="e.hyss.lbracket" highlight="brackets" />
<element id="e.hyss.lparen" pattern="(" starts_block="1" highlight="brackets" block_name="Parentheses block" />
<element pattern=")" ends_block="1" blockstartelement="e.hyss.lparen" highlight="brackets" />
<element id="e.hyss.lcomment" pattern="/*" starts_block="1" block_name="C style comment" highlight="hyss-comment">
	<context symbols="*/&#9;&#10;&#13;" highlight="hyss-comment">
		<element pattern="*/" ends_block="1" blockstartelement="e.hyss.lcomment" highlight="hyss-comment" ends_context="1" />
	</context>
</element>

<element id="e.hyss.heredoc_string" pattern="&lt;&lt;&lt;EOF" starts_block="1" highlight="brackets" >
<reference><b>heredoc string</b>
<span color="red">Warning</span>: only <b>&lt;&lt;&lt;EOF</b> (without space) is correctly highligted.</reference>
	<autocomplete enable="1" />
	<context symbols="&#13;&#10;EF$ " highlight="hyss-heredoc-string">
		<element pattern="$[a-zA-Z_][a-zA-Z0-9_]*" is_regex="1" case_insens="1" highlight="hyss-variable" />
		<element pattern="(&#10;|&#13;|&#13;&#10;)EOF"  is_regex="1" ends_block="1" ends_context="1" blockstartelement="e.hyss.heredoc_string" highlight="brackets" />
	</context>
</element>
<element id="e.hyss.nowdoc_string" pattern="&lt;&lt;&lt;'EOF'" starts_block="1" highlight="brackets" >
<reference><b>nowdoc string</b>
<span color="red">Warning</span>: only <b>&lt;&lt;&lt;'EOF'</b> (without space) is correctly highligted</reference>
	<autocomplete enable="1" />
	<context symbols="&#13;&#10;EF" highlight="hyss-heredoc-string">
		<element pattern="(&#10;|&#13;|&#13;&#10;)EOF"  is_regex="1" ends_block="1" ends_context="1" blockstartelement="e.hyss.nowdoc_string" highlight="brackets" />
	</context>
</element>

<element id="e.hyss.variable" pattern="$[a-zA-Z_][a-zA-Z0-9_]*" is_regex="1" case_insens="1" highlight="hyss-variable" identifier_mode="2" identifier_autocomp="option:autocomplete_HYSS_variable"/>
<element pattern="&#34;" highlight="string" id="e.hyss.doublequotedstring">
	<context symbols="\&#34;$ &#9;&#10;&#13;' -&lt;&gt;{}[]" highlight="string">
		<element pattern="\\." highlight="string" is_regex="1" />
<!--		<element pattern="\$" highlight="string" />-->
		<element idref="e.hyss.variable" />
		<group class="sql-in-hyss-string" highlight="hyss-string-sql-keyword" case_insens="1">
			<element pattern="AND" />
			<element pattern="AS" />
			<element pattern="ASC" />
			<element pattern="BY" />
			<element pattern="COUNT" />
			<element pattern="DATE" />
			<element pattern="DATE_FORMAT" />
			<element pattern="DELETE" />
			<element pattern="DESC" />
			<element pattern="DISTINCT" />
			<element pattern="FROM" />
			<element pattern="GROUP" />
			<element pattern="IN" />
			<element pattern="INNER" />
			<element pattern="INSERT" />
			<element pattern="INTO" />
			<element pattern="JOIN" />
			<element pattern="LEFT" />
			<element pattern="LIMIT" />
			<element pattern="NOT" />
			<element pattern="ON" />
			<element pattern="OR" />
			<element pattern="ORDER" />
			<element pattern="REGEXP" />
			<element pattern="SELECT" />
			<element pattern="SET" />
			<element pattern="UPDATE" />
			<element pattern="VALUES" />
			<element pattern="WHERE" />
		</group>
		<element pattern="&#34;" highlight="string" ends_context="1" ends_block="1" blockstartelement="e.hyss.doublequotedstring"/>
	</context>
</element>
<element pattern="//" highlight="hyss-comment">
	<context id="c.hyss.linecomment" symbols="!&#125;&#10;&#13;" highlight="hyss-comment">
		<element pattern="(&#10;|&#13;|&#13;&#10;)" is_regex="1" ends_context="1" />
		<element pattern="!&#125;" ends_context="2" ends_block="1" blockstartelement="e.hyss.open" highlight="hyss-block-tag" />
	</context>
</element>
<!-- there is a bug in the scanning engine such that a regex style pattern like (#|//) won't work. The reason is that
there is special code if a pattern ends on a symbol. That code fails on the above pattern because both # and / are ends
for this pattern and both of them are a symbol. That's why we have a separate entry for # and for // -->
<element pattern="#" highlight="hyss-comment">
	<context idref="c.hyss.linecomment"/>
</element>
<element pattern="!&#125;" ends_block="1" blockstartelement="e.hyss.open" highlight="hyss-block-tag" ends_context="1" />
</context></element>
</group>

&all-html;

</context>
</definition>
</owlgallang>
